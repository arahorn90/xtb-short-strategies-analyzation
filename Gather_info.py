from API import XTB
import pandas
from time import sleep

# TODO write general solution to gather data.
options = []

API = XTB("14984698", "Nikita123")


def get_candles_data(symbol):
    # print("symbol: " + symbol)
    candles = API.get_candles("M5", symbol, qty_candles=5000)
    df_item = pandas.DataFrame.from_records(candles)
    print(df_item)
    return df_item


def save_in_csv(stocks):
    for stock in stocks:
        sleep(0.21) # XBT turn connection off if we send requests so frequently. Here is a workaround with sleep
        try:
            stock_candles = get_candles_data(stock)
            stock_candles.to_csv(f"gathered_data\\{stock}.csv")
            #print(stock_candles)
        except:
            print(f"Failed stock {stock}")


def get_all_symbols():
    #API = login_xtb(delta_time)
    symbols = API.get_AllSymbols()
    df_item = pandas.DataFrame.from_records(symbols['returnData'])
    #logout_xtb(API)
    # print(symbols)
    return df_item

def set_options():
    all_symbols = get_all_symbols()
    print(all_symbols)
    for elem in all_symbols.index:
        options.append(all_symbols["symbol"][elem])


set_options()

# for stock in options:
    #stock_candles = update(stock["value"])
save_in_csv(options)

#save_in_csv(["USDCAD"])

API.logout()


