from API import XTB
import pandas
import dash
from dash import html
from dash import dcc
from datetime import datetime
from dash.dependencies import Output, Input, State
import logging
from time import sleep


logger = logging.getLogger("XTB")
options = []

def login_xtb(delta_time = ""):
    return XTB("14984698", "Nikita123", delta_time)


def logout_xtb(API):
    API.logout()


API = login_xtb()
delta_time = API.get_delta_time()

def update(symbol):
    # TODO for now connection is lost, because of requests frequency. It should be fixed.
    #API = login_xtb(delta_time)
    print("symbol: " + symbol)
    candles = API.get_candles("M5", symbol, qty_candles=10)
    df_item = pandas.DataFrame.from_records(candles)
    print(df_item)
    #logout_xtb(API)
    return df_item


def get_all_symbols():
    #API = login_xtb(delta_time)
    symbols = API.get_AllSymbols()
    df_item = pandas.DataFrame.from_records(symbols['returnData'])
    #logout_xtb(API)
    # print(symbols)
    return df_item


def set_options():
    all_symbols = get_all_symbols()
    print(all_symbols)
    for elem in all_symbols.index:
        options.append({"label": all_symbols["description"][elem],
                        "value": all_symbols["symbol"][elem]})


def set_html_page():
    app = dash.Dash()
    app.layout = html.Div([
        html.H1("Stock Ticker Web App"),
        html.Div([
            html.H2("Select a stock:"),
            dcc.Dropdown(
                id="dropdown",
                options=options,
                value=[],
                multi=True)
        ]),
        html.Div([
            #html.H2("Select Date")#,
            #dcc.DatePickerRange(
            #    id="datepicker",
            #    min_date_allowed=datetime(2023, 7, 5),
            #    max_date_allowed=datetime.today(),
            #    start_date=datetime(2023, 7, 5),
            #    end_date=datetime.today()
            #)
        ]),
        html.Div([
            html.Button(
                id="submit-button",
                n_clicks=0,
                children="Submit"
            )
        ]),
        dcc.Graph(
            id="stock-graph"
        )
    ])

    @app.callback(Output("stock-graph", "figure"),
                  [Input("submit-button", "n_clicks")],
                  [State("dropdown", "value"),
                   #State("datepicker", "start_date"),
                   #State("datepicker", "end_date")
                   ]
                  )
    def update_graph(number_of_clicks, stocks):
        data = []
        if stocks:
            logger.error("update_graph start ")
            logger.error(stocks)

        for stock in stocks:
            sleep(0.21)
            stock_candles = update(stock)
            logger.warning(stock_candles)

            data.append({
                "x": stock_candles["datetime"],
                "y": stock_candles["open"],
                "name": stock
            })
        figure = {
            "data": data,
            "layout": {
                "title": "Stock data"}
        }
        return figure

    logger.warning("Set run server")
    app.run_server()


# while new_name != 'q':
# Ask the user for a name.
logger.warning("Set options")
set_options()
logger.warning("Set html")
set_html_page()
# API.logout()
